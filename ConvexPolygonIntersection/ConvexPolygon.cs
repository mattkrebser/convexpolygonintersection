﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ConvexPolygonIntersection
{
    public class ConvexPolygon2D
    {
        /// <summary>
        /// Ordered list of points for the ConvexPolygon2D.
        /// </summary>
        public List<Vector2d> Vertices;

        /// <summary>
        /// All edges in this polygon
        /// </summary>
        public IEnumerable<Edge> Edges
        {
            get
            {
                if (Vertices == null || Vertices.Count < 2) yield break;
                for (int i = 0; i+1 < Vertices.Count; i++)
                    yield return new Edge(Vertices[i], Vertices[i + 1]);
                yield return new Edge(Vertices[Vertices.Count - 1], Vertices[0]);
            }
        }

        /// <summary>
        /// Make a new convex polygon with the input set of ordered point. 
        /// Points are ordered as follows: v0v1 = edge0, v1v2 = edge1, v2v3 = edge2,...vn-1vn = edgen-1
        /// </summary>
        /// <param name="Verts"></param>
        public ConvexPolygon2D(List<Vector2d> Verts)
        {
            if (Verts == null || Verts.Count < 3)
                throw new System.Exception("Error, insufficient vertices");
            Vertices = Verts;
        }

        static System.Random randGen;
        /// <summary>
        /// Returns a new Randomized ConvexPolygon
        /// </summary>
        /// <param name="minPoints"></param>
        /// <param name="maxPoints"></param>
        /// <returns></returns>
        public static ConvexPolygon2D RandomPolygon(System.Drawing.Rectangle bounds, 
            int minPoints=3, int maxPoints=32)
        {
            if (randGen == null)
                randGen = new Random();
            if (maxPoints < minPoints || minPoints < 3 || maxPoints < 3)
                throw new System.Exception("Invalid number of points range");

            maxPoints = randGen.Next(minPoints, maxPoints);

            //generate random points
            List<Vector2d> pointList = new List<Vector2d>();
            for (int i = 0; i < maxPoints; i++)
            {
                Vector2d point = new Vector2d(randGen.NextDouble() * bounds.Width + bounds.X,
                    randGen.NextDouble() * bounds.Height + bounds.Y);
                pointList.Add(point);
            }

            //find bottom most point
            double miny = double.MaxValue;
            int index = 0;
            for (int i = 0; i < pointList.Count; i++)
            {
                if (pointList[i].y < miny)
                {
                    miny = pointList[i].y;
                    index = i;
                }
            }

            //graham scan
            List<Tuple<double, Vector2d>> byangle = new List<Tuple<double, Vector2d>>();
            for (int i = 0; i < pointList.Count; i++)
            {
                if (i != index)
                    byangle.Add(new Tuple<double, Vector2d>(Vector2d.Angle(pointList[index],
                        pointList[i]), pointList[i]));
            }
            //sort by angle
            var points = new LinkedList<Vector2d>(byangle.OrderBy(x => x.v1).Select(y => y.v2));
            points.AddFirst(pointList[index]);

            //add points to list
            var node = points.First;
            while (node.Next.Next != null)
            {
                Vector2d a = node.Value;
                Vector2d b = node.Next.Value;
                Vector2d c = node.Next.Next.Value;

                if (!MathI.Left(c, a, b))
                {
                    points.Remove(node.Next);
                    if (node.Previous != null)
                        node = node.Previous;
                }
                else
                    node = node.Next;
            }
            //handle last point (c# linked list isnt allowed to be circular)
            Vector2d u = node.Value;
            Vector2d v = node.Next.Value;
            Vector2d t = pointList[index];
            if (!MathI.Left(t, u, v))
                points.Remove(node.Next);
            //make counter clock wise order
            var l = new List<Vector2d>(points); l.Reverse();
            return new ConvexPolygon2D(l);
        }

        /// <summary>
        /// Brute force convex polygon intersection
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public bool IntersectsBruteForce(ConvexPolygon2D c)
        {
            if (Vertices == null || Vertices.Count < 3 || c == null || c.Vertices == null || c.Vertices.Count < 3)
                throw new System.Exception("Input Polygon Error");

            //if either polygon contains a point of the other
            foreach (var v in c.Vertices)
            {
                if (ContainsPoint(v))
                    return true;
            }
            //if either polygon contains a point of the other
            foreach (var v in Vertices)
            {
                if (c.ContainsPoint(v))
                    return true;
            }
            //if any of the edges intersect eachother
            foreach (var e1 in c.Edges)
            {
                foreach (var e2 in Edges)
                {
                    if (MathI.LineSegLineSegIntersection(e1.v1, e1.v2, e2.v1, e2.v2))
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Orourke method for convex polygon intersection. Linear Time algorithm
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public bool Intersects(ConvexPolygon2D c, bool bresult)
        {
            int ai = 0, bi = 0;
            do
            {
                //get edge vectors and edge direction vectors
                //A,B edge directions a1,a2,b1,b2 edge end points
                //a1 is the the vectoor at [a-1], b1 is the vector at [b-1]
                Vector2d 
                    a1 = Vertices[(ai + Vertices.Count - 1) % Vertices.Count],
                    a = Vertices[ai % Vertices.Count], 
                    b1 = c.Vertices[(bi + c.Vertices.Count - 1) % c.Vertices.Count], 
                    b = c.Vertices[bi % c.Vertices.Count];
                Vector2d A = a - a1;
                Vector2d B = b - b1;
                //compute if the edges are pointed at eachother
                double cross = -Vector2d.Cross(A, B);
                //compute half plane values
                bool bHalfA = MathI.Left(b, a, a1);
                bool aHalfB = MathI.Left(a, b, b1);
                //if intersects, then done
                if (Intersects(new Edge(a1, a), new Edge(b1, b)))
                    return true;
                //otherwise cases
                else
                {
                    if (cross >= 0)
                    {
                        if (bHalfA)
                            ai++;
                        else
                            bi++;
                    }
                    else
                    {
                        if (aHalfB)
                            bi++;
                        else
                            ai++;
                    }
                }

            } while ((ai < Vertices.Count || bi < Vertices.Count) &&
            (ai < Vertices.Count * 2 && bi < c.Vertices.Count * 2));

            //at this point, the only other intersection case is where one polygon
            //encapsulates the other entirely
            //otherwise the polygons are disjoint
            //If one polygon intersects the other, then for all edges on one polygon, all edges are left of any vertice
            int left_count = 0;
            foreach (var e in Edges)
                left_count += !MathI.Left(c.Vertices[0], e.v1, e.v2) ? 1 : 0;
            int right_count = 0;
            foreach (var e in c.Edges)
                right_count += !MathI.Left(Vertices[0], e.v1, e.v2) ? 1 : 0;

            var result = left_count == Vertices.Count || right_count == c.Vertices.Count? true : false;

            return result;
        }

        /// <summary>
        /// Two edges intersect?
        /// </summary>
        /// <param name="e1"></param>
        /// <param name="e2"></param>
        /// <returns></returns>
        bool Intersects(Edge e1, Edge e2)
        {
            return MathI.LineSegLineSegIntersection(e1.v1, e1.v2, e2.v1, e2.v2);
        }

        /// <summary>
        /// Returns true if this ConvexPolygon contains the input point inside of it
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public bool ContainsPoint(Vector2d p)
        {
            foreach (var e in Edges)
            {
                if (MathI.Left(p, e.v1, e.v2))
                    return false;
            }
            return true;
        }

        struct Tuple<T,T1>
        {
            public T v1;
            public T1 v2;
            public Tuple(T t, T1 t1)
            {
                v1 = t; v2 = t1;
            }
        }

        public override string ToString()
        {
            string s = "ConvexPolygon:{";
            if (Vertices != null)
                for (int i = 0; i < Vertices.Count; i++)
                {
                    s += Vertices[i].ToStringShort() + ",";
                }
            s += "}";
            return s;
        }

        /// <summary>
        /// Structure representing an edge
        /// </summary>
        public struct Edge
        {
            public Vector2d v1;
            public Vector2d v2;
            public Edge(Vector2d a, Vector2d b)
            {
                v1 = a;v2 = b;
            }
        }
    }
}
