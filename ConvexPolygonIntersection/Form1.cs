﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ConvexPolygonIntersection
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Number of tests run
        /// </summary>
        int NumTests = 0;
        /// <summary>
        /// Number of failed tests
        /// </summary>
        int NumWrong = 0;

        Graphics graphics;
        Rectangle bbox;

        List<ConvexPolygon2D> Polygons = new List<ConvexPolygon2D>();
        List<Color> pColors = new List<Color>();

        static Form1 form;
        /// <summary>
        /// Singleton instance
        /// </summary>
        public static Form1 Instance
        {
            get
            {
                if (form == null)
                    form = new Form1();
                return form;
            }
        }

        Form1()
        {
            InitializeComponent();
            graphics = this.CreateGraphics();
            this.Paint += Initialize;
        }

        void Initialize(object sender, PaintEventArgs e)
        {
            bbox = new Rectangle(10, 10, 500, 500);
            Redraw();
        }

        /// <summary>
        /// Redraw the graphic.
        /// </summary>
        void Redraw()
        {
            //draw surrounding box
            graphics.DrawRectangle(Pens.Black, bbox);
            //draw polygons
            if (Polygons != null)
            {
                for (int i = 0; i < Polygons.Count; i++)
                {
                    DrawPolygon(Polygons[i], pColors[i]);
                }
            }
        }

        /// <summary>
        /// Draw a polygon with the input color
        /// </summary>
        /// <param name="p"></param>
        /// <param name="c"></param>
        public static void DrawPolygon(ConvexPolygon2D p, Color c)
        {
            Pen pen = new Pen(c, 1);
            for (int i = 0; i < p.Vertices.Count; i++)
            {
                if (i + 1 < p.Vertices.Count)
                    Instance.graphics.DrawLine(pen, (Point)p.Vertices[i], (Point)p.Vertices[i + 1]);
                else
                    Instance.graphics.DrawLine(pen, (Point)p.Vertices[i], (Point)p.Vertices[0]);
            }
        }
        /// <summary>
        /// Draw a line segment
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="c"></param>
        public static void DrawLine(Vector2d p1, Vector2d p2, Color c)
        {
            Pen pen = new Pen(c, 1);
            Instance.graphics.DrawLine(pen, (Point)p1, (Point)p2);
        }
        /// <summary>
        /// Run button!
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RunButton_Click(object sender, EventArgs e)
        {
            Random rand = new System.Random();
            string trials = NumberOfTrialsTextBox.Text;
            int numtrials;

            try
            {
                numtrials = Int32.Parse(trials);
            }
            catch
            {
                numtrials = 1;
            }

            if (numtrials <= 0)
                numtrials = 1;

            try
            {
                for (int i = 0; i < numtrials; i++)
                {
                    //increment number of tests
                    NumTests++;
                    //make bounding boxes to spawn random points in
                    int x1 = rand.Next(0, bbox.Width / 2);
                    int y1 = rand.Next(0, bbox.Width / 2);
                    int x2 = rand.Next(0, bbox.Width / 2);
                    int y2 = rand.Next(0, bbox.Width / 2);
                    Rectangle r1 = new Rectangle(x1 + bbox.X, y1 + bbox.Y, bbox.Width/2, bbox.Height/2);
                    Rectangle r2 = new Rectangle(x2 + bbox.X, y2 + bbox.Y, bbox.Width/2, bbox.Height/2);
                    //create randomized polygons
                    ConvexPolygon2D p1 = ConvexPolygon2D.RandomPolygon(r1);
                    ConvexPolygon2D p2 = ConvexPolygon2D.RandomPolygon(r2);
                    //clear old polygon structures
                    Polygons.Clear();
                    pColors.Clear();
                    //add new polygons
                    Polygons.Add(p1);
                    Polygons.Add(p2);
                    pColors.Add(Color.Red);
                    pColors.Add(Color.Blue);
                    //clear background
                    graphics.Clear(Control.DefaultBackColor);
                    //compute intersection
                    bool BruteForceResult = p1.IntersectsBruteForce(p2);
                    IntersectsTextBox.Text = BruteForceResult ? "Intersects!" : "Doesn't Intersect";
                    //Log results in console
                    Console.WriteLine("Brute Force Test " + NumTests.ToString() + ": " + 
                        (BruteForceResult ? "Intersects" : "Doesn't Intersect"));
                    //ORourke intersection
                    bool ORourkeResult = p1.Intersects(p2, BruteForceResult);
                    Console.WriteLine("O'Rourke Test " + NumTests.ToString() + ": " +
                        (ORourkeResult ? "Intersects" : "Doesn't Intersect"));
                    ORourkeResultTextBox.Text = ORourkeResult ? "Intersects!" : "Doesn't Intersect";
                    //determine if correct
                    NumWrong += ORourkeResult == BruteForceResult ? 0 : 1;

                    //redraw
                    Redraw();
                }
                Console.WriteLine("Total Failed Tests: " + NumWrong.ToString());
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
