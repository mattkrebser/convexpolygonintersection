﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConvexPolygonIntersection
{
    public static class MathI
    {
        public const double rad2deg = 180 / Math.PI;

        /// <summary>
        /// Point left of line test. positive For clockwise order
        /// </summary>
        /// <param name="point"></param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool Left(Vector2d point, Vector2d a, Vector2d b)
        {
            return ((b.x - a.x) * (point.y - a.y) - (b.y - a.y) * (point.x - a.x)) > 0;
        }

        /// <summary>
        /// Line segment line segment intersection
        /// </summary>
        /// <param name="a1"></param>
        /// <param name="a2"></param>
        /// <param name="b1"></param>
        /// <param name="b2"></param>
        /// <returns></returns>
        public static bool LineSegLineSegIntersection(Vector2d a, Vector2d b, Vector2d c, Vector2d d)
        {
            double s = 0, t = 0;
            double denom = 0;

            denom = a.x * (d.y - c.y) + b.x * (c.y - d.y) + c.x * (a.y - b.y) + d.x * (b.y - a.y);

            if (denom==0)
            {
                if (!Collinear(a, b, c)) return false;
                if (Between(a, b, c)) return true;
                if (Between(a, b, d)) return true;
                if (Between(c, d, a)) return true;
                if (Between(c, d, b)) return true;
                return false;
            }

            s = (a.x * (d.y - c.y) + c.x * (a.y - d.y) + d.x * (c.y - a.y)) / denom;
            t = (-(a.x * (c.y - b.y) + b.x * (a.y - c.y) + c.x * (b.y - a.y))) / denom;

            return s >= 0 && s <= 1 && t >= 0 && t <= 1;
        }

        static bool Between(Vector2d a, Vector2d b, Vector2d c)
        {
            if (a.x != b.x)
                return (a.x <= c.x && c.x <= b.x) || (a.x >= c.x && c.x >= b.x);
            else
                return (a.y <= c.y && c.y <= b.y) || (a.y >= c.y && c.y >= b.y);
        }
        public static bool Collinear(Vector2d a, Vector2d b, Vector2d c)
        {
            return a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y) == 0;
        }
    }


    /// <summary>
    /// two coordinated double precision vector
    /// </summary>
    [System.Serializable]
    public struct Vector2d
    {
        public double x;
        public double y;

        public Vector2d(double X, double Y)
        {
            x = X; y = Y;
        }

        public static Vector2d zero
        {
            get
            {
                return new Vector2d(0, 0);
            }
        }

        public bool Equals(Vector2d v)
        {
            return v.x == x && v.y == y;
        }
        public override bool Equals(object obj)
        {
            return obj is Vector2d ? Equals((Vector2d)obj) : false;
        }
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 29;
                hash = hash * 17 + x.GetHashCode();
                hash = hash * 19 + x.GetHashCode();
                return hash;
            }
        }
        public static bool operator ==(Vector2d v1, Vector2d v2)
        {
            return v1.Equals(v2);
        }
        public static bool operator !=(Vector2d v1, Vector2d v2)
        {
            return !(v1 == v2);
        }

        public static double Distance(Vector2d a, Vector2d b)
        {
            return Math.Sqrt((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
        }
        public override string ToString()
        {
            return "Vector2d(" + x.ToString() + "," + y.ToString() + ")";
        }
        public string ToStringShort()
        {
            return "(" + x.ToString() + "," + y.ToString() + ")";
        }
        public Vector2d normalized
        {
            get
            {
                double mag = magnitude;
                return new Vector2d(x / mag, y / mag);
            }
        }
        public double magnitude
        {
            get
            {
                return System.Math.Sqrt(x * x + y * y);
            }
        }
        public double sqrMagnitude
        {
            get
            {
                return x * x + y * y;
            }
        }

        public static Vector2d operator -(Vector2d a, Vector2d b)
        {
            return new Vector2d(a.x - b.x, a.y - b.y);
        }
        public static Vector2d operator -(Vector2d a)
        {
            return new Vector2d(-a.x, -a.y);
        }
        public static Vector2d operator +(Vector2d a, Vector2d b)
        {
            return new Vector2d(a.x + b.x, a.y + b.y);
        }
        public static Vector2d operator *(Vector2d a, double b)
        {
            return new Vector2d(a.x * b, a.y * b);
        }
        public static Vector2d operator *(double b, Vector2d a)
        {
            return new Vector2d(a.x * b, a.y * b);
        }
        public static Vector2d operator /(Vector2d a, double b)
        {
            return new Vector2d(a.x / b, a.y / b);
        }

        public static double Angle(Vector2d a, Vector2d b)
        {
            Vector2d c = b - a;
            double angle = Math.Atan2(c.y, c.x) * MathI.rad2deg;
            if (angle < 0)
                angle = 360 + angle;
            return angle;
        }
        public static double Cross(Vector2d a, Vector2d b)
        {
            return a.x * b.y - b.x * a.y;
        }

        public static Vector2d right
        {
            get
            {
                return new Vector2d(1, 0);
            }
        }
        public static Vector2d left
        {
            get
            {
                return new Vector2d(-1, 0);
            }
        }
        public static Vector2d up
        {
            get
            {
                return new Vector2d(0, 1);
            }
        }
        public static Vector2d down
        {
            get
            {
                return new Vector2d(0, -1);
            }
        }

        public static explicit operator System.Drawing.Point(Vector2d v)
        {
            return new System.Drawing.Point((int)v.x, (int)v.y);
        }
    }
}
