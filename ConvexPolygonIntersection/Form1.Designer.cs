﻿namespace ConvexPolygonIntersection
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RunButton = new System.Windows.Forms.Button();
            this.NumberOfTrialsTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.IntersectsTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ORourkeResultTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // RunButton
            // 
            this.RunButton.Location = new System.Drawing.Point(548, 486);
            this.RunButton.Name = "RunButton";
            this.RunButton.Size = new System.Drawing.Size(75, 23);
            this.RunButton.TabIndex = 0;
            this.RunButton.Text = "Run";
            this.RunButton.UseVisualStyleBackColor = true;
            this.RunButton.Click += new System.EventHandler(this.RunButton_Click);
            // 
            // NumberOfTrialsTextBox
            // 
            this.NumberOfTrialsTextBox.Location = new System.Drawing.Point(548, 460);
            this.NumberOfTrialsTextBox.Name = "NumberOfTrialsTextBox";
            this.NumberOfTrialsTextBox.ShortcutsEnabled = false;
            this.NumberOfTrialsTextBox.Size = new System.Drawing.Size(100, 20);
            this.NumberOfTrialsTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(548, 441);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Number Of Trials";
            // 
            // IntersectsTextBox
            // 
            this.IntersectsTextBox.Location = new System.Drawing.Point(551, 26);
            this.IntersectsTextBox.Name = "IntersectsTextBox";
            this.IntersectsTextBox.ReadOnly = true;
            this.IntersectsTextBox.Size = new System.Drawing.Size(100, 20);
            this.IntersectsTextBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(551, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Brute Force:";
            // 
            // ORourkeResultTextBox
            // 
            this.ORourkeResultTextBox.Location = new System.Drawing.Point(551, 68);
            this.ORourkeResultTextBox.Name = "ORourkeResultTextBox";
            this.ORourkeResultTextBox.ReadOnly = true;
            this.ORourkeResultTextBox.Size = new System.Drawing.Size(100, 20);
            this.ORourkeResultTextBox.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(554, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "O\'Rourke";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 521);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ORourkeResultTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.IntersectsTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.NumberOfTrialsTextBox);
            this.Controls.Add(this.RunButton);
            this.Name = "Form1";
            this.Text = "Convex Polygon Intersection";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button RunButton;
        private System.Windows.Forms.TextBox NumberOfTrialsTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox IntersectsTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ORourkeResultTextBox;
        private System.Windows.Forms.Label label3;
    }
}

