# README #

### What is this repository for? ###

Convex Polygon Intersection using Brute force and O'Rourke intersection method.
Convex polygons are randomly generated using graham scan and visualized using windows form app.

### How do I get set up? ###

* Download project and open in visual studio.
* This project was made with visual studio community 2015 .NET version 4.6


![img.png](https://bitbucket.org/repo/d4ga4R/images/2478481472-img.png)